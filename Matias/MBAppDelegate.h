//
//  MBAppDelegate.h
//  Matias
//
//  Created by Cristián Arenas on 3/11/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
