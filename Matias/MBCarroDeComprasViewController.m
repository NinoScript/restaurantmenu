//
//  MBCarroDeComprasViewController.m
//  Matias
//
//  Created by Cristián Arenas on 3/11/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBCarroDeComprasViewController.h"
#import "MBModelo.h"
#import "MBUtilities.h"

@interface MBCarroDeComprasViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem* pedirButton;
@property (strong, nonatomic) NSMutableArray* carroMutable;
@end

@implementation MBCarroDeComprasViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    self.carroMutable = [NSMutableArray array];

    NSDictionary* c = [MBModelo sharedModelo].carroDeCompras;
    for (NSDictionary* p in [c allKeys]) {
        [self.carroMutable addObject:@{
                                        producto : p, cantidad : c[p]
                                     }];
    }

    [self.tableView reloadData];
}

- (void)updateModel
{
    NSMutableDictionary* c = [NSMutableDictionary dictionary];
    for (NSDictionary* p in self.carroMutable) {
        c[p[producto]] = p[cantidad];
    }
    [MBModelo sharedModelo].carroDeCompras = c;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [self.carroMutable count];
    self.pedirButton.enabled = count > 0;
    return count;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ruid"
                                                            forIndexPath:indexPath];
    NSDictionary* fila = self.carroMutable[indexPath.row];
    NSDictionary* p = fila[producto];
    NSNumber* c = fila[cantidad];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ × %@", p[nombre], c];
    NSUInteger subtotal = [c integerValue] * [p[precio] integerValue];
    cell.detailTextLabel.text = [[MBUtilities priceFormatter] stringFromNumber:@(subtotal)];

    return cell;
}

- (UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ruid_footer"];

    NSUInteger total = 0;
    for (NSDictionary* fila in self.carroMutable) {
        NSDictionary* p = fila[producto];
        NSNumber* c = fila[cantidad];
        NSUInteger subtotal = [c integerValue] * [p[precio] integerValue];
        total += subtotal;
    }
    cell.textLabel.text = @"Total";
    cell.detailTextLabel.text = [[MBUtilities priceFormatter] stringFromNumber:@(total)];

    return cell;
}

- (BOOL)tableView:(UITableView*)tableView canEditRowAtIndexPath:(NSIndexPath*)indexPath
{
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView*)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.carroMutable removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[
                                             indexPath
                                          ]
                         withRowAnimation:UITableViewRowAnimationFade];
        [self updateModel];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

@end
