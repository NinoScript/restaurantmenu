//
//  MBCartaViewController.m
//  Matias
//
//  Created by Cristián Arenas on 3/11/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBCartaViewController.h"
#import "MBDetalleViewController.h"
#import "MBUtilities.h"

@interface MBCartaViewController ()
@property (nonatomic, strong) NSArray* carta;
@end

@implementation MBCartaViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.carta = @[
        @{
           seccion : @"jamón",
           productos : @[
               @{
                  nombre : @"con queso",
                  precio : @2000
               },
               @{
                  nombre : @"sin queso",
                  precio : @1500
               }
           ]
        },
        @{
           seccion : @"café",
           productos : @[
               @{
                  nombre : @"solo",
                  precio : @1000
               },
               @{
                  nombre : @"cortado",
                  precio : @1200
               },
               @{
                  nombre : @"espresso",
                  precio : @2000
               },
               @{
                  nombre : @"ristreto",
                  precio : @2500
               }
           ]
        }
    ];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return [self.carta count];
}

- (NSString*)tableView:(UITableView*)tableView
    titleForHeaderInSection:(NSInteger)section
{
    return self.carta[section][seccion];
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section
{
    return [self.carta[section][productos] count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ruid"
                                                            forIndexPath:indexPath];
    NSDictionary* producto = [self productoForRowAtIndexPath:indexPath];
    cell.textLabel.text = producto[nombre];
    cell.detailTextLabel.text
        = [[MBUtilities priceFormatter] stringFromNumber:producto[precio]];

    return cell;
}

- (NSDictionary*)seccionForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return self.carta[indexPath.section];
}
- (NSDictionary*)productoForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return [self seccionForRowAtIndexPath:indexPath][productos][indexPath.row];
}

- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [self performSegueWithIdentifier:@"IrADetalle"
                              sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue
                 sender:(NSIndexPath*)indexPath
{
    MBDetalleViewController* detalle = (id)segue.destinationViewController;
    NSDictionary* producto = [self productoForRowAtIndexPath:indexPath];
    detalle.title = [@[
        [self seccionForRowAtIndexPath:indexPath][seccion],
        [self productoForRowAtIndexPath:indexPath][nombre]
    ] componentsJoinedByString:@" "];
    detalle.producto = producto;
}
@end