//
//  MBDetalleViewController.m
//  Matias
//
//  Created by Cristián Arenas on 3/11/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBDetalleViewController.h"
#import <LoremPixel/UIImageView+LoremPixel.h>
#import "MBModelo.h"

@interface MBDetalleViewController ()
@property (weak, nonatomic) IBOutlet UIImageView* imagen;
@property (weak, nonatomic) IBOutlet UILabel* numero;
@property (weak, nonatomic) IBOutlet UIStepper* stepper;

@property (assign, nonatomic) NSUInteger cantidad;
@end

@implementation MBDetalleViewController

- (void)setCantidad:(NSUInteger)cantidad
{
    _cantidad = cantidad;

    self.numero.text = [NSString stringWithFormat:@"%lu", (unsigned long)cantidad];
    self.stepper.value = cantidad;
}

- (void)viewDidLoad
{
    NSData* dummyTextData = [self.title dataUsingEncoding:NSASCIIStringEncoding
                                     allowLossyConversion:YES];
    NSString* dummyTextString = [[NSString alloc] initWithData:dummyTextData
                                                      encoding:NSASCIIStringEncoding];
    NSString* dummyText = [dummyTextString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];

    [self.imagen getDummyImageForCategory:LoremPixelCategoryFood
                                   inGray:NO
                            withDummyText:dummyText];
    NSDictionary* carro = [MBModelo sharedModelo].carroDeCompras;
    NSNumber* cantidad = carro[self.producto];
    self.cantidad = [cantidad integerValue];
}
- (IBAction)valorCambio:(UIStepper*)sender
{
    self.cantidad = [sender value];
}
- (IBAction)comprar:(id)sender
{
    NSMutableDictionary* carro = [[MBModelo sharedModelo].carroDeCompras mutableCopy];
    carro[self.producto] = @(self.cantidad);
    [MBModelo sharedModelo].carroDeCompras = carro;

    [self.navigationController popViewControllerAnimated:YES];
}

@end
