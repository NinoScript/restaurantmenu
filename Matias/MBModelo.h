//
//  MBModelo.h
//  Matias
//
//  Created by Cristián Arenas on 3/11/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBModelo : NSObject
@property (strong) NSDictionary* carroDeCompras;
+ (instancetype)sharedModelo;
@end
