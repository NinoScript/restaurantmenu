//
//  MBModelo.m
//  Matias
//
//  Created by Cristián Arenas on 3/11/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBModelo.h"

@implementation MBModelo

+ (instancetype)sharedModelo
{
    static MBModelo* instancia;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instancia = [self new];
    });

    return instancia;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.carroDeCompras = [NSDictionary dictionary];
    }
    return self;
}

@end
