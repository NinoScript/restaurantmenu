//
//  MBPedirViewController.m
//  Matias
//
//  Created by Cristián Arenas on 4/17/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBPedirViewController.h"
#import "MBModelo.h"
#import <TKScanKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface MBPedirViewController () <TKScanningProviderDelegate, MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) TKScanningProvider* scanningProvider;
@property (strong, nonatomic) NSString* scannedTitle;
@end

@implementation MBPedirViewController

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.scanningProvider = [TKScanKit newProviderWithName:@"ZBarSDK"];
        self.scanningProvider.delegate = self;
    }
    return self;
}
- (IBAction)pedirEnMesaAction:(id)sender
{
    [self.scanningProvider presentScannerFromViewController:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.scannedTitle) {
        MFMailComposeViewController* controller = [MFMailComposeViewController new];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Compra a la mesa"];
        NSString* mensaje
            = [NSString stringWithFormat:@"El código de la mesa es: %@\n\n"
                                          "Quiero comprar lo siguiente: %@",
                                         self.scannedTitle, [MBModelo sharedModelo].carroDeCompras];
        [controller setMessageBody:mensaje
                            isHTML:NO];
        [controller setToRecipients:@[
                                       @"matias.camacho.p@facebook.com"
                                    ]];
        [controller setCcRecipients:@[
                                       @"cristian.arenas@facebook.com"
                                    ]];
        if (controller)
            [self presentViewController:controller
                               animated:YES
                             completion:nil];

        self.scannedTitle = nil;
    }
}

- (void)scanningProvider:(TKScanningProvider*)provider
    didFinishScanningWithText:(NSString*)text
                         info:(NSDictionary*)info
{
    self.scannedTitle = text;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        [MBModelo sharedModelo].carroDeCompras = [NSDictionary new];
    }
    [controller dismissViewControllerAnimated:YES
                                   completion:^{
        [self.navigationController popViewControllerAnimated:YES];
                                   }];
}

- (void)scanningProviderDidCancel:(TKScanningProvider*)provider
{
    NSLog(@"scanner cancelled");
}

- (void)scanningProvider:(TKScanningProvider*)provider
    didFailedScanningWithError:(NSError*)error
{
    [self showMessageWithTitle:@"Error"
                          text:[error localizedDescription]];
}

- (void)showMessageWithTitle:(NSString*)title
                        text:(NSString*)text
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

@end
