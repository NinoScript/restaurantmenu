//
//  MBTabBarViewController.h
//  Matias
//
//  Created by Cristián Arenas on 4/17/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBTabBarViewController : UITabBarController

@end
