//
//  MBTabBarViewController.m
//  Matias
//
//  Created by Cristián Arenas on 4/17/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBTabBarViewController.h"
#import "MBModelo.h"

@interface MBTabBarViewController ()

@end

@implementation MBTabBarViewController

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [[MBModelo sharedModelo] addObserver:self
                                  forKeyPath:@"carroDeCompras"
                                     options:(NSKeyValueObservingOptionNew
                                              | NSKeyValueObservingOptionInitial)
                                     context:nil];
    }
    return self;
}

- (void)dealloc
{
    [[MBModelo sharedModelo] removeObserver:self
                                 forKeyPath:@"carroDeCompras"];
}

- (void)observeValueForKeyPath:(NSString*)keyPath
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    UITabBarItem* carroDeCompras = self.tabBar.items[1];
    NSUInteger total = 0;
    for (NSNumber* cantidad in [[MBModelo sharedModelo].carroDeCompras allValues]) {
        total += [cantidad integerValue];
    }
    carroDeCompras.badgeValue = total == 0 ? nil : [NSString stringWithFormat:@"%@", @(total)];
}
@end
