//
//  MBUtilities.h
//  Matias
//
//  Created by Cristián Arenas on 4/17/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* const seccion = @"seccion";
static NSString* const productos = @"productos";
static NSString* const nombre = @"nombre";
static NSString* const precio = @"precio";

static NSString* const producto = @"nombreConPrecio";
static NSString* const cantidad = @"cantidadEnCarro";

@interface MBUtilities : NSObject
+ (NSNumberFormatter*)priceFormatter;
@end
