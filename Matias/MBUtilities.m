//
//  MBUtilities.m
//  Matias
//
//  Created by Cristián Arenas on 4/17/14.
//  Copyright (c) 2014 NiñoScript. All rights reserved.
//

#import "MBUtilities.h"

@implementation MBUtilities
+ (NSNumberFormatter*)priceFormatter
{
    static NSNumberFormatter* formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSNumberFormatter new];
        formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    });
    return formatter;
}

@end
